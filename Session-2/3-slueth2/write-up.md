# Slueth
[challaneg file](https://mercury.picoctf.net/static/9061ae8456a4ff51098c5183d910a080/dds2-alpine.flag.img.gz) 

## Slueth
[The Sleuth Kit®](http://www.sleuthkit.org/sleuthkit/) (TSK) is a library and collection of command line tools that allow you to investigate disk images. The core functionality of TSK allows you to analyze volume and file system data. The library can be incorporated into larger digital forensics tools and the command line tools can be directly used to find evidence. 

## Solution
they are asking foe a file named 'down-at-the-bottom.txt'
analyzing the file with autopsy and searching for the file solver the challange.