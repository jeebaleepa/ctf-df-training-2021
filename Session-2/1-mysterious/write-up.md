# Mysterious

## File Carving
Files-within-files is a common trope in forensics CTF challenges, and also in embedded systems' firmware where primitive or flat filesystems are common. The term for identifying a file embedded in another file and extracting it is "file carving." One of the best tools for this task is the firmware analysis tool binwalk and foremost. 

## Solution
we use either :
```bash
binwalk -e mysterious.jpeg
```
or :
```bash
foremost mysterious.jpeg
```
we find a zip file we extract it and we find a whole bunch of files .
```strings *``` with ```grep``` did the trick.
```bash
strings * | grep -i flag
```
