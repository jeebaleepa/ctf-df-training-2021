from PIL import Image                  
import codecs                                                     
                                                              
# rot 13 function
def rot13(s): return codecs.getencoder("rot-13")(s)[0]      


# read image
im = Image.open('not_art.png')
pixels = im.load()
w, h = im.size


# convert from base3 to base 10 
def converter(number, base):
    # split number in figures
    figures = [int(i, base) for i in str(number)]
    # invert oder of figures (lowest count first)
    figures = figures[::-1]
    result = 0
    # loop over all figures
    for i in range(len(figures)):
        # add the contirbution of the i-th figure
        result += figures[i]*base**i
    return result

# check if pixels inside the image
def inside(x, y):
    return (x >= 0 and x < w and y >= 0 and y < h)

# check if black
def black(x):
    return (x[0] + x[1] + x[2] == 0)


vis = dict() # visited 
path = [] # path list

# movements lists but moving 10 pixels at a time to not repeat the same step because every color square is 10*10
mx = [0, 10, 0, -10] 
my = [-10, 0, 10, 0]

# typical dfs function but considring black pixels to be obstacles
def dfs(i, j):
    vis[(i, j)] = 1
    path.append(pixels[i, j])
    if i == 150 and j == 150:
        return
    for k in range(4):
        ni = i + mx[k]
        nj = j + my[k]
        if inside(ni, nj) and (ni, nj) not in vis.keys() and not black(pixels[ni, nj]):
            dfs(ni, nj)


dfs(0, 0)

base3 = []

# convert path pixels to base3
mp = {255: '2', 192: '1', 0: '0'}
for i in range(len(path)):
    p = path[i]
    base3.append(str(mp[p[0]])+str(mp[p[1]])+str(mp[p[2]]))
a1z26 = ''

# convert back to lettters 
for i in range(len(base3)):
    num = converter(int(base3[i]), 3)
    if num != 0:
        a1z26 += (chr(num+96))


print(a1z26)

print('------------------------------------------------------------------------')

print(rot13(a1z26))
