# Meow 

## Steghide 
[Steghide](http://steghide.sourceforge.net/)  is  a  steganography  program  that is able to hide data in various kinds of image- and audio-files. The color- respectivly sample-frequencies are not changed thus making the embedding resistant against first-order statistical tests.

## [Stegcracker](https://github.com/Paradoxis/StegCracker)
Steganography brute-force utility to uncover hidden data inside files.

## Solution

Mount the disk :
```
mount disk.img mountpoint
```

copy the files somewhere else , then use steghide in each image with no passowrds :
```
steghide extract -sf [filename]
```

ok this is a large base64 divided into parts .
we combine the files and decode the base64 to see it is a jpg image.

use stegcracker and rockyou to find the flag : 
```
stegcracker new.jpg rockyou.txt
```