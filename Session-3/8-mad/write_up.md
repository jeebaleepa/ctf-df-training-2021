# Mad
## Esoteric programming language
An esoteric programming language or esolang, is a computer programming language designed to experiment with weird ideas, to be hard to program in, or as a joke, rather than for practical use.
Here is a [list of esolangs](https://esolangs.org/wiki/Language_list).

## Solution 
extract hidden files with steghide 
we found a weird string . 
doing some research we find that this is code in [COW](https://esolangs.org/wiki/COW)
we use [this interprter](http://www.frank-buss.de/cow.html) to find the flag.
