# unsplash.it
## LSB 
The most common and easy way to do steganography is using the [Least Significant Bit](https://itnext.io/steganography-101-lsb-introduction-with-python-4c4803e08041).

## PILLOW 
[PIL](https://pillow.readthedocs.io/en/stable/reference/index.html) is a python library for manipulatiing images.
## Solution

We use this script to solve tha challange 


```
from PIL import Image
from Crypto.Util.number import long_to_bytes as ltb
img = Image.open('splash.png')
pixels = img.load()
w , h = img.size
lsb = ''
for i in range(w):
    for j in range(h):
        pixel=pixels[i,j]
        r = str(pixel[0]&1)
        g = str(pixel[1]&1)
        b = str(pixel[2]&1)
        lsb += r+g+b

flag = int(lsb,2)
print(ltb(flag))


```
