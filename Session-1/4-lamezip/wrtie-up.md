# Lamezip

## Nested zips

this is a common challnge in CTF's in which there are too many zips inside each other , and wee need to write a script to solve it .

## solution

we see that every zip require a password.
so we use this script and the given wordlist to solve the challange.

````
from zipfile import ZipFile
import os
passwd=[]
with open('wordlist','r') as file :
    passwd = file.read().split('\n')

for i in range(1000,0,-1):
    with ZipFile(str(i)+'.zip') as zf:
        for p in passwd :
            try :
                zf.extractall(pwd=p.encode())
                print(str(i)+'.zip : right password' ,p)
                break
            except :
                pass
    os.remove(str(i)+'.zip')```
````
