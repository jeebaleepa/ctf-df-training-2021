# Jazz

## Filesystem Files

Occasionally, a CTF forensics challenge consists of a full disk image, and the player needs to have a strategy for finding a needle (the flag) in this haystack of data.

## diff command
```diff``` is a tool in linux to find the differences between files

## solution

we need to mount this img data .
we need a mount point so we create a directory called files.

```bash
sudo mount myfiles.img files
```

we navigate to the files directory and list the files :

```bash
total 11824
drwxrwxrwx 3 root  root      4096 Mar 16 13:40  .
drwxrwxr-x 3 jeeba jeeba     4096 Mar 25 23:37  ..
-rwxrwxrwx 1 jeeba jeeba 10849543 Apr  9  2020 '01. A1 - Give Life Back To Music.mp3'
-rw-r--r-- 1 jeeba jeeba   590491 Dec 14 00:28  albaath.png
-rw-rw-r-- 1 jeeba jeeba    55361 Oct 16 08:31  Bo.txt
-rw-rw-r-- 1 jeeba jeeba    55393 Oct 16 08:31  B.txt
-rw-r--r-- 1 jeeba jeeba   138545 Dec 18 18:33  cmd1.gif
-rw-r--r-- 1 jeeba jeeba    79985 Dec 18 18:56  cmd1.png
-rw-r--r-- 1 jeeba jeeba   110187 Dec 18 15:41  grub.png
-rw-r--r-- 1 jeeba jeeba   189720 Dec 18 19:23  htop.png
drwx------ 2 root  root     16384 Jan 30 19:37  lost+found

```

we browse the file and try some stuff , we finally notice that the files ```B.txt``` and ```Bo.txt```  are kinda the same ,  so we use the ```diff``` command to see if they differ .
They do , and there are some additional characters in ```B.txt```. 
so we some work we find the flag
