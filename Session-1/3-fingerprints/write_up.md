# Fingerprints

## Strings Command

the `strings` command is a tool to print all printable charachters in a file.
and it is a first step in every ctf forensics challange.

## Base64

Base 64 is a numerical system that uses 64 charachters [a-zA-Z0-9+/=] , it is used as a way to represent data ([wikipedia](https://en.wikipedia.org/wiki/Base64)).

## Solution

we `strings` all the images we have . to see that
image 3.jpg has what looks like base64 at he end of it so we take it and decode it so see some data with the word `rar` in it .
we save the file and name it 3.rar
we use the file command to ensure this is a RAR file, and it is .

```bash
file 3.rar
3.rar: RAR archive data, v5
```

but it needs a password .
we use our friend [john the ripper](https://github.com/openwall/john) and the [rockyou.txt](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwjMl6zyrszvAhW9QhUIHeRBDBUQFjAAegQIAxAD&url=https%3A%2F%2Fgithub.com%2Fbrannondorsey%2Fnaive-hashcat%2Freleases%2Fdownload%2Fdata%2Frockyou.txt&usg=AOvVaw3snAERl1mU6Ccr4WFEazBd) to brute force the password.
we type these commands :

````bash
 JohnTheRipper/run/rar2john 3.rar > hash
 JohnTheRipper/run/john hash --wordlist=rockyou.txt
 JohnTheRipper/run/john hash --show```
````
we see the password komputer

