# Jian-yang

## Magic Bytes
every file has what is called a magic bytes (or file signature) that denotes the file type .
they appear at the start of every file .

## File command 
the file command in linux is a tool that reads the magic bytes of a file and determines the file type .

when we try this command on our file we have this output :
```
file jian-yang
jian-yang: data
```
this means that the file command couldn't find a known file type for this file .
so there must be something wrong with the magic bytes.

## Hex Editors
Hex editors are tools that allows us to open a file and see its hex values (or bytes).
every byte is represented with two hexadecimal digits .
there are so many hex editors out there :
- xxd : a command line hex editor in linux .
- Ghex :  a GUI hex editor in linux .
- Hxd : a GUI hex editor for windows .

in this example i'll be using Ghex

Also we should note the endianess of the file : [see on wikipedia](https://en.wikipedia.org/wiki/Endianness)

## Solution
when we open the file with ghex we see that the first two bytes are : 
```
BA DA
```
and we see the word  ```jfif``` this tells us that this file was originally a jpeg file 
we search [online](https://www.garykessler.net/library/file_sigs.html) for the magic bytes for the jpeg format we found that they are :
```
FF D8
```
so we change first two magic bytes to to FF D8.
boom we found the flag .

**flag{ur_a_fat_n_a_poor}**