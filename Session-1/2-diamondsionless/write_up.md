# Diamondsionless

## PNG File Strucutre
every file has a structure . [png file structrue](http://www.libpng.org/pub/png/spec/1.2/PNG-Contents.html) 

## Solution
we open the file we found an error with the crc (we can use the pngcheck tool in linux).
after reading the png specs we notice that the width and height in the IHDR chunk data are ```00 00 00 FF```, hence the diamensions are tampered with , so we need to find the dimensions for this image .
we see that the crc for the IHDR are still there so we need to brute force the dimensions so the the crc calculates to ```08 06 D6 BD``` .
we use this script :
```
from zlib import crc32
from pwn import p32
target = 0x0806d6bd
header = bytes.fromhex("49484452000000FF000000FF0806000000")


def check(w, h, header):
    w_hex = p32(w)[::-1]
    h_hex = p32(h)[::-1]
    header = header.replace(b"\x00\x00\x00\xFF" +
                            b"\x00\x00\x00\xFF", w_hex+h_hex)
    if crc32(header) == target:
        print(w, " ", h)
        print(w_hex.hex(), " ", h_hex.hex())
        exit()


for x in range(2000):
    for y in range(2000):
        check(x, y, header)

```
we found the dimensions to be ```0000028d   00000218```.
we modify this in a hex editor to fix the image and find the flag .