# CTF-DF-Training-2021

### **This is a repo for all the stuff and challanges discussed in the CTF Digital Forensics Training Camp 2021 at Al-Hadara-Group based in Syria**.

## Resources 
- **How To Install Kali** : https://www.edureka.co/blog/how-to-install-kali-linux/
- **Python3 Tutorial** : https://www.tutorialspoint.com/python/index.htm
- **Forensics Guide** :https://trailofbits.github.io/ctf/forensics/
- **CyberChef** :https://gchq.github.io/CyberChef/
- **File's signatures** : https://www.garykessler.net/library/file_sigs.html
- **Volitality Commands**: https://github.com/volatilityfoundation/volatility/wiki/Command-Reference
- **Steganography guide** : https://0xrick.github.io/lists/stego/
- **stego-toolkit** : https://github.com/DominicBreuker/stego-toolkit
- **How Does Stegsolve Work** : https://www.youtube.com/watch?v=fkCZxOI_w-c&t=482s
## TryHackMe Rooms

- **Linux Fundamentals Part 1** :https://tryhackme.com/room/linux1
- **Linux Fundamentals Part 2** :https://tryhackme.com/room/linux2
- **Linux Fundamentals Part 3** :https://tryhackme.com/room/linux3
- **Volitality** : https://tryhackme.com/room/bpvolatility
- **Overpass 2 - Hacked** : https://tryhackme.com/room/overpass2hacked

## Sessions Screencasts :
- **Session 1** : https://drive.google.com/file/d/1jk6L_U3Cz4bZmqao_fUKIXAkVcDJVQoR/view?usp=sharing
- **Session 2** : https://drive.google.com/file/d/1IVxsNj9j13MMw7Y_ZF4xkUGHuW-ZMH7x/view?usp=sharing
- **Session 3** : https://drive.google.com/file/d/1OK0Pi9EywlUh82_q0ssAuBIhJZqhNTPO/view?usp=sharing
